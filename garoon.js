/*★arg:日付arg:担当者★*/
function sendGaroon(event,dateFiled,userFiled){
    var record=event.record;
	if(event.type!=="app.record.edit.submit"){
		return;
	}
	var scheduleInfo = []; //SOAP連携で使用する連想配列
    //*********************
    // scheduleInfo['domain']に本番のサブドメインを設定して下さい。
    //*********************
    scheduleInfo['domain'] = 'visionary'; //サブドメイン
    scheduleInfo['action'] = 'UtilGetRequestToken';
    scheduleInfo['token'] = null;
    //トークンの取得に失敗した場合はガルーンに一度もアクセスしておらず、認証Cookieを取得できていない可能性があるので
    //一度ガルーンにアクセスして、認証Cookieを取得してから再度トークンを取得
    if (scheduleInfo['token'] == null) {
      accessGaroon();
      if (!getRequestToken(scheduleInfo)) { //トークンを取得
        //event.error = scheduleInfo['errmsg'];
        alert(scheduleInfo['errmsg']);
        return event;
      }
    }
	var userIds = [];
    var postXML = postSchedule('ScheduleAddEvents', scheduleInfo);
    scheduleInfo['action'] = 'BaseGetUsersByLoginName';
    var user=record[userFiled]["value"];//担当者
    var date=record[dateFiled]["value"];//日付
    //担当者と日付が登録されていない場合、処理を実行しない
    if(user.length===0 || !date){
        alert("フィールド"+userFiled+"と"+dateFiled+"を入力してください")
    }
    var momentDate=moment(date).format('YYYY-MM-DDTHH:mm:ssZ');
    for(var i=0;i<user.length;i++){
        var member = user[i]["code"];
        scheduleInfo['grnLoginName'] = member;
        userIds.push(getUserIds(scheduleInfo, 1));
   }
   scheduleInfo['action'] = 'ScheduleAddEvents';
   scheduleInfo['scheduleId'] = '0';
   postXML += addSchedule(record, userIds, scheduleInfo, momentDate);
   postXML += '</parameters></ScheduleAddEvents></SOAP-ENV:Body></SOAP-ENV:Envelope>';
   putGaroon(postXML).done(function (xml) {});
}
//スケジュールに登録するパラメーターを設定する
function addSchedule(record, userIds,obj,date) {
  obj['start'] = date
  obj['member'] = '';
  for(var i=0;i<userIds.length;i++){
      obj['member'] += '<user id="' + userIds[i] + '"></user>';
  }
  obj['plan'] = "";
  obj['title'] = "テスト";
  //スケジュールのメモ欄に該当のレコードのURLを張り付ける
  var url = 'https://' + obj['domain'] + '.cybozu.com/k/' + kintone.app.getId() + "/show#record="+record["$id"]["value"]
  obj['description'] = url
  var xmlDate = '<schedule_event xmlns="" id="' + obj['scheduleId'] + '" event_type="normal" version="0" public_type="public" plan="' + obj['plan'] + '" detail="' + obj['title'] + '" description="' + obj['description'] + '" timezone="Asia/Tokyo" end_timezone="Asia/Tokyo" allday="true" start_only="true">';
  xmlDate += '<members><member>' + obj['member'] + '</member></members>'
  xmlDate += '<when><date start="' + obj['start'] + '" end="' + obj['start'] + '"></date></when></schedule_event>';
  return xmlDate;
}
//ログイン名からガルーンのユーザー内部IDを取得する
function getUserIds(obj, flag) {
  var xmlhttp = getXmlHttp();
  obj['grnLoginNameElement'] = '';
  if (flag == 1) {
    obj['grnLoginNameElement'] += '<login_name xmlns="">' + obj['grnLoginName'] + '</login_name>'
  } else {
    for (var i = 0; i < obj['grnLoginName'].length; i++) {
      obj['grnLoginNameElement'] += '<login_name xmlns="">' + obj['grnLoginName'][i]['code'] + '</login_name>';
    }
  }
  var url = "/g/cbpapi/base/api.csp";
  var xml = createXML(obj);
  xmlhttp.open("POST", url, false);
  xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  xmlhttp.setRequestHeader("Content-Type", "text/xml; charset=UTF-8");
  xmlhttp.send(xml);
  var userIds;
  if (xmlhttp.status == 200) {
    var userElements;
    if (xmlhttp.responseXML.childNodes[0] == null) { //IE
      var objXML = parseXML(xmlhttp.responseText);
      userElements = objXML.getElementsByTagName('returns')[0];
      for (var i = 0; i < userElements.childNodes.length; i++) {
        if (userElements.childNodes[i].nodeName == "user") {
          for (var j = 0; j < userElements.childNodes[i].attributes.length; j++) {
            if (userElements.childNodes[i].attributes[j].name == 'key') {
              userIds = userElements.childNodes[i].attributes[j].value;
            }
          }
        }
      }
    } else {
      userElements = xmlhttp.responseXML.childNodes[0].childNodes[3].childNodes[1].childNodes[1].childNodes;
      for (var i = 0; i < userElements.length; i++) {
        if (userElements[i].localName == "user") {
          for (var j = 0; j < userElements[i].attributes.length; j++) {
            if (userElements[i].attributes[j].name == 'key') {
              userIds = userElements[i].attributes[j].value;
            }
          }
        }
      }
    }
    return userIds;
  } else { //削除失敗
    obj['errmsg'] = '[ERROR]:\n ユーザー名の取得に失敗しました。\n code:' + xmlhttp.status + '\n text:' + xmlhttp.statusText + '\n response:' + xmlhttp.responseText;
    return null;
  }
}
//ガルーンからスケジュールを削除する
function deleteSchedule(obj) {
  var deleteDate = '<event_id xmlns="">' + obj['scheduleId'] + '</event_id>';
  return deleteDate;
}
//ガルーンからリクエストトークンを取得
function getRequestToken(obj) {
  var xmlhttp = getXmlHttp();
  var url = "/g/util_api/util/api.csp";
  var xml = createXML(obj);
  xmlhttp.open("POST", url, false);
  xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  xmlhttp.setRequestHeader("Content-Type", "text/xml; charset=UTF-8");
  xmlhttp.send(xml);
  //トークン取得処理
  if (xmlhttp.status == 200) {
    if (xmlhttp.responseXML.childNodes[0] == null) { //IE
      var objXML = parseXML(xmlhttp.responseText);
      obj['token'] = objXML.getElementsByTagName('request_token')[0].text;
      if (obj['token'] === undefined) { //IE9以降？
        obj['token'] = objXML.getElementsByTagName('request_token')[0].textContent;
      }
    } else {
      obj['token'] = xmlhttp.responseXML.childNodes[0].childNodes[3].childNodes[1].childNodes[1].childNodes[1].textContent;
    }
    return true;
  } else { //取得失敗
    obj['errmsg'] = '[ERROR]:\n トークンの取得に失敗しました。\n code:' + xmlhttp.status + '\n text:' + xmlhttp.statusText + '\n response:' + xmlhttp.responseText;
    return false;
  }
}
//トークン取得に失敗した場合、擬似的にガルーンにアクセスする
function accessGaroon() {
  var xmlhttp = getXmlHttp();
  var url = "/g/";
  xmlhttp.open("GET", url, false);
  xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  xmlhttp.setRequestHeader("Content-Type", "text/xml; charset=UTF-8");
  xmlhttp.send();
  if (xmlhttp.status == 200) {
    return;
  }
}
//xmlhttpオブジェクトを生成
function getXmlHttp() {
  var xmlhttp = false;
  if (typeof ActiveXObject != "undefined") { //IE
    try {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (e) {
      xmlhttp = false;
    }
  }
  if (!xmlhttp && typeof XMLHttpRequest != "undefined") { //IE以外
    xmlhttp = new XMLHttpRequest();
  }
  return xmlhttp;
}

function parseXML(html) {
  if (window.DOMParser) {
    var parser = new DOMParser();
    var xmlDoc = parser.parseFromString(html, "text/xml");
  } else { // Internet Explorer
    var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
    xmlDoc.async = "false";
    xmlDoc.loadXML(html);
  }
  return xmlDoc;
}

function createXML(obj) {
  var xml = '';
  //スケジュール用SOAPヘッダー
  var soapHeaderSchedule = '<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:schedule_services="http://wsdl.cybozu.co.jp/schedule/2008"><SOAP-ENV:Header><Action SOAP-ENV:mustUnderstand="1" xmlns="http://schemas.xmlsoap.org/ws/2003/03/addressing">';
  //ベース用SOAPヘッダー
  var soapHeaderBase = '<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:base_services="http://wsdl.cybozu.co.jp/base/2008"><SOAP-ENV:Header><Action SOAP-ENV:mustUnderstand="1" xmlns="http://schemas.xmlsoap.org/ws/2003/03/addressing">';
  //Util用SOAPヘッダー
  var soapHeaderUtil = '<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:util_api_services="http://wsdl.cybozu.co.jp/util_api/2008"><SOAP-ENV:Header> <Action SOAP-ENV:mustUnderstand="1" xmlns="http://schemas.xmlsoap.org/ws/2003/03/addressing">';
  //Action指定後のSOAPヘッダー
  var soapHeaderAfter = '</Action><Timestamp SOAP-ENV:mustUnderstand="1" Id="id" xmlns="http://schemas.xmlsoap.org/ws/2002/07/utility"><Created>2037-08-12T14:45:00Z</Created> <Expires>2037-08-12T14:45:00Z</Expires></Timestamp> <Locale>jp</Locale></SOAP-ENV:Header>'
  //Action名毎にXMLを作成
  switch (obj['action']) {
    //ユーザーID取得
  case 'BaseGetUsersByLoginName':
    xml = soapHeaderBase + obj['action'] + soapHeaderAfter;
    xml += '<SOAP-ENV:Body><' + obj['action'] + '><parameters>' + obj['grnLoginNameElement'] + '</parameters></' + obj['action'] + '></SOAP-ENV:Body></SOAP-ENV:Envelope>';
    break;
    //トークン取得    
  case 'UtilGetRequestToken':
    xml = soapHeaderUtil + obj['action'] + soapHeaderAfter;
    xml += '<SOAP-ENV:Body><' + obj['action'] + '></' + obj['action'] + '></SOAP-ENV:Body></SOAP-ENV:Envelope>';
    break;
  }
  return xml;
}

function getSchedule(date, userIds) {
  var m = moment(date).utc();
  var start_datetime = m.format('YYYY-MM-DDTHH:mm:ssZ');
  var end_datetime = m.add(1, 'days').add(-1, 'seconds').format('YYYY-MM-DDTHH:mm:ssZ');
  var str = "";
  //参加者に設定するユーザーIDを設定
  str += '<user id="' + userIds + '"></user>';
  var data = '<?xml version="1.0" encoding="UTF-8"?>';
  data += '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope">';
  data += '  <soap:Header>';
  data += '    <Action>';
  data += '      ScheduleGetEventsByTarget';
  data += '    </Action>';
  data += '    <Security>';
  data += '      <UsernameToken>';
  data += '        <Username></Username>';
  data += '        <Password></Password>';
  data += '      </UsernameToken>';
  data += '    </Security>';
  data += '    <Timestamp>';
  data += '      <Created>2010-08-12T14:45:00Z</Created>';
  data += '      <Expires>2037-08-12T14:45:00Z</Expires>';
  data += '    </Timestamp>';
  data += '    <Locale>jp</Locale>';
  data += '  </soap:Header>';
  data += '  <soap:Body>';
  data += '    <ScheduleGetEventsByTarget>';
  data += '      <parameters start=\"' + start_datetime + '\" end=\"' + end_datetime + '\">' + str + '</parameters>';
  data += '    </ScheduleGetEventsByTarget>';
  data += '  </soap:Body>';
  data += '</soap:Envelope>';
  return $.ajax({
    method: 'POST',
    url: '/g/cbpapi/schedule/api?',
    data: data,
    dataType: 'xml',
    contentType: 'text/xml',
    async: false
  });
}
//ガルーンへスケジュールを登録するxmlの共通部分を作成
//戻り値：xml形式の文字列
function postSchedule(action, obj) {
  //スケジュール用SOAPヘッダー
  var soapHeaderSchedule = '<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:schedule_services="http://wsdl.cybozu.co.jp/schedule/2008"><SOAP-ENV:Header><Action SOAP-ENV:mustUnderstand="1" xmlns="http://schemas.xmlsoap.org/ws/2003/03/addressing">';
  //Action指定後のSOAPヘッダー
  var soapHeaderAfter = '</Action><Timestamp SOAP-ENV:mustUnderstand="1" Id="id" xmlns="http://schemas.xmlsoap.org/ws/2002/07/utility"><Created>2037-08-12T14:45:00Z</Created> <Expires>2037-08-12T14:45:00Z</Expires></Timestamp> <Locale>jp</Locale></SOAP-ENV:Header>'
  var data = soapHeaderSchedule + action + soapHeaderAfter
  data += '  <SOAP-ENV:Body>';
  data += '    <' + action + '>';
  data += '      <parameters><request_token xmlns="">' + obj['token'] + '</request_token>';
  return data;
}
//ガルーンへスケジュールをPOSTする（同期処理）
function putGaroon(data) {
  return $.ajax({
    method: 'POST',
    url: '/g/cbpapi/schedule/api?',
    data: data,
    dataType: 'xml',
    contentType: 'text/xml',
    async: false
  });
}