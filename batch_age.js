/**
 * Javascriptをcmdで実行するテストファイル
 * 19年後までの誕生日をGaroonスケジュールに登録
 **/
var SUBDOMAIN="";//ｻﾌﾞﾄﾞﾒｲﾝ名
var dbg = 1; //デバッグ時：1、運用時：0
function getRecord(app, token) {
  var offset = 0;
  var records = [];
  var loopendflg = false;
  var today=new Date();
  var month=today.getMonth()+1;
  var date=today.getDate();
  //レコードの全件取得
  while (!loopendflg) {
    var query = encodeURIComponent('limit 500 offset ' + offset);
    var appUrl = 'https://' + SUBDOMAIN + '.cybozu.com/k/v1/records.json' + '?app=' + app + '&query=' + query;
    // 同期リクエストを行う
    var xmlHttp = WScript.CreateObject("MSXML2.XMLHTTP"); //=new XMLHttpRequest()
    xmlHttp.open("GET", appUrl, false);
    //xmlHttp.setRequestHeader('X-Cybozu-Authorization',account);
    xmlHttp.setRequestHeader('X-Cybozu-API-Token', token);
    xmlHttp.send(null);
    if (xmlHttp.status !== 200) {
      WScript.echo(xmlHttp.responseText);
      logText += '失敗,0,' + new Date() + ',"' + ERROR_TEXT_1 + '"';
      file.WriteLine(logText);
      file.Close();
      fs = null;
      return;
    }
    //取得したレコードをArrayに格納
    var respdata = JSON.parse(xmlHttp.responseText);
    if (respdata.records.length > 0) {
      for (var i = 0; respdata.records.length > i; i++) {
        records.push(respdata.records[i]);
      }
      offset += respdata.records.length;
    } else {
      loopendflg = true;
    }
  }
  if (dbg) WScript.echo("records: " + records.length);
  var putAry=[];
  for(var i=0;i<records.length;i++){
    
  	var birthDay=records[i]["date_BirthDay"]["value"];//生年月日
  	var flg=[];
  	if(!birthDay){
  		continue;
  	}
  	var birthYear=birthDay.substr(0,4);//年
  	var birthMonth=birthDay.substr(5,2);//月
  	var birthDate=birthDay.substr(8,2);//日
  	var date=birthYear + "/" + birthMonth + "/" + birthDate;　//yyyy/mm/ddに形式を書き換える
  	if (dbg) WScript.echo("birth: " + date);
  	var objDate=new Date(date);//日杖オブジェクトに変換
  	var age=calcAge(date);
  	if (dbg) WScript.echo("年齢" + age);
  	var putParam={
  		id:records[i]["$id"]["value"],
  		record:{
  			/*chk_birthFlg:{
  				value:flg
  			},*/
  			num_age:{
  				value:age
  			}
  		}
  	}
  	putAry.push(putParam);
  }
  putAllRecordsByToken(putAry,app,token);
  WScript.echo("Successfully updated age");
}
//日付のゼロ埋めを行う
function paddingZero(num,digit){
	return ('0000' + num).slice(-1 * digit);
}
//誕生日を計算
function calcAge(date){
	// ゼロパディングするための関数
	//const paddingZero = (num, digit) => ('0000' + num).slice(-1 * digit);
	// あなたの誕生日
	var birth = new Date(date); // ここに誕生日を書いてください
	var y2 = paddingZero(birth.getFullYear(), 4);
	var m2 = paddingZero(birth.getMonth() + 1, 2);
	var d2 = paddingZero(birth.getDate(), 2);
	// 今日の日付
	var today = new Date();
	var y1 = paddingZero(today.getFullYear(), 4);
	var m1 = paddingZero(today.getMonth() + 1, 2);
	var d1 = paddingZero(today.getDate(), 2);
	var age = Math.floor((Number(y1 + m1 + d1) - Number(y2 + m2 + d2)) / 10000);
	return age;

}
//レコードの全件更新を実施
function putAllRecordsByToken(data, opt_appId, opt_token) {
  var appId = opt_appId
  var token = opt_token
  var putData = data.slice(0, 100);
  var nextData = data.slice(100);
  var url = 'https://' + SUBDOMAIN + '.cybozu.com/k/v1/records.json';
  var body = {
    app: appId,
    records: putData
  }
  // 同期リクエストを行う
  var xhr = WScript.CreateObject("MSXML2.XMLHTTP");
  xhr.open('PUT', url, false);
  xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  if (token) {
    xhr.setRequestHeader('X-Cybozu-API-Token', token);
  }
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify(body));
  var resp = JSON.parse(xhr.responseText);
  if (xhr.status === 200) {
    if (nextData.length) {
      return putAllRecordsByToken(nextData, appId, token);
    }
    return true;
  } else {
    /*alert("レコードを更新できません。" + resp.message);
    console.log(resp);
    return false;*/
  }
}
// ------------------------------------------------------------
// バイナリデータの文字列から Base64 文字列に変換する関数 (同期実行)
// ------------------------------------------------------------
function Base64_From_StringOfBinaryData(binary) {
  var dic = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'];
  var base64 = "";
  var num = binary.length;
  var n = 0;
  var b = 0;
  var i = 0;
  while (i < num) {
    b = binary.charCodeAt(i);
    if (b > 0xff) return null;
    base64 += dic[(b >> 2)];
    n = (b & 0x03) << 4;
    i++;
    if (i >= num) break;
    b = binary.charCodeAt(i);
    if (b > 0xff) return null;
    base64 += dic[n | (b >> 4)];
    n = (b & 0x0f) << 2;
    i++;
    if (i >= num) break;
    b = binary.charCodeAt(i);
    if (b > 0xff) return null;
    base64 += dic[n | (b >> 6)];
    base64 += dic[(b & 0x3f)];
    i++;
  }
  var m = num % 3;
  if (m) {
    base64 += dic[n];
  }
  if (m == 1) {
    base64 += "==";
  } else if (m == 2) {
    base64 += "=";
  }
  return base64;
}
//json2.js
//start
if (typeof JSON !== "object") {
  JSON = {};
}
(function () {
  "use strict";
  var rx_one = /^[\],:{}\s]*$/;
  var rx_two = /\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g;
  var rx_three = /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g;
  var rx_four = /(?:^|:|,)(?:\s*\[)+/g;
  var rx_escapable = /[\\\"\u0000-\u001f\u007f-\u009f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
  var rx_dangerous = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;

  function f(n) {
    // Format integers to have at least two digits.
    return n < 10 ? "0" + n : n;
  }

  function this_value() {
    return this.valueOf();
  }
  if (typeof Date.prototype.toJSON !== "function") {
    Date.prototype.toJSON = function () {
      return isFinite(this.valueOf()) ? this.getUTCFullYear() + "-" + f(this.getUTCMonth() + 1) + "-" + f(this.getUTCDate()) + "T" + f(this.getUTCHours()) + ":" + f(this.getUTCMinutes()) + ":" + f(this.getUTCSeconds()) + "Z" : null;
    };
    Boolean.prototype.toJSON = this_value;
    Number.prototype.toJSON = this_value;
    String.prototype.toJSON = this_value;
  }
  var gap;
  var indent;
  var meta;
  var rep;

  function quote(string) {
    //If the string contains no control characters, no quote characters, and no
    //backslash characters, then we can safely slap some quotes around it.
    //Otherwise we must also replace the offending characters with safe escape
    //sequences.
    rx_escapable.lastIndex = 0;
    return rx_escapable.test(string) ? "\"" + string.replace(rx_escapable, function (a) {
      var c = meta[a];
      return typeof c === "string" ? c : "\\u" + ("0000" + a.charCodeAt(0).toString(16)).slice(-4);
    }) + "\"" : "\"" + string + "\"";
  }

  function str(key, holder) {
    //Produce a string from holder[key].
    var i; // The loop counter.
    var k; // The member key.
    var v; // The member value.
    var length;
    var mind = gap;
    var partial;
    var value = holder[key];
    //If the value has a toJSON method, call it to obtain a replacement value.
    if (value && typeof value === "object" && typeof value.toJSON === "function") {
      value = value.toJSON(key);
    }
    //If we were called with a replacer function, then call the replacer to
    //obtain a replacement value.
    if (typeof rep === "function") {
      value = rep.call(holder, key, value);
    }
    //What happens next depends on the value's type.
    switch (typeof value) {
    case "string":
      return quote(value);
    case "number":
      //JSON numbers must be finite. Encode non-finite numbers as null.
      return isFinite(value) ? String(value) : "null";
    case "boolean":
    case "null":
      //If the value is a boolean or null, convert it to a string. Note:
      //typeof null does not produce "null". The case is included here in
      //the remote chance that this gets fixed someday.
      return String(value);
      //If the type is "object", we might be dealing with an object or an array or
      //null.
    case "object":
      //Due to a specification blunder in ECMAScript, typeof null is "object",
      //so watch out for that case.
      if (!value) {
        return "null";
      }
      //Make an array to hold the partial results of stringifying this object value.
      gap += indent;
      partial = [];
      //Is the value an array?
      if (Object.prototype.toString.apply(value) === "[object Array]") {
        //The value is an array. Stringify every element. Use null as a placeholder
        //for non-JSON values.
        length = value.length;
        for (i = 0; i < length; i += 1) {
          partial[i] = str(i, value) || "null";
        }
        //Join all of the elements together, separated with commas, and wrap them in
        //brackets.
        v = partial.length === 0 ? "[]" : gap ? "[\n" + gap + partial.join(",\n" + gap) + "\n" + mind + "]" : "[" + partial.join(",") + "]";
        gap = mind;
        return v;
      }
      //If the replacer is an array, use it to select the members to be stringified.
      if (rep && typeof rep === "object") {
        length = rep.length;
        for (i = 0; i < length; i += 1) {
          if (typeof rep[i] === "string") {
            k = rep[i];
            v = str(k, value);
            if (v) {
              partial.push(quote(k) + (gap ? ": " : ":") + v);
            }
          }
        }
      } else {
        //Otherwise, iterate through all of the keys in the object.
        for (k in value) {
          if (Object.prototype.hasOwnProperty.call(value, k)) {
            v = str(k, value);
            if (v) {
              partial.push(quote(k) + (gap ? ": " : ":") + v);
            }
          }
        }
      }
      //Join all of the member texts together, separated with commas,
      //and wrap them in braces.
      v = partial.length === 0 ? "{}" : gap ? "{\n" + gap + partial.join(",\n" + gap) + "\n" + mind + "}" : "{" + partial.join(",") + "}";
      gap = mind;
      return v;
    }
  }
  //If the JSON object does not yet have a stringify method, give it one.
  if (typeof JSON.stringify !== "function") {
    meta = { // table of character substitutions
      "\b": "\\b",
      "\t": "\\t",
      "\n": "\\n",
      "\f": "\\f",
      "\r": "\\r",
      "\"": "\\\"",
      "\\": "\\\\"
    };
    JSON.stringify = function (value, replacer, space) {
      //The stringify method takes a value and an optional replacer, and an optional
      //space parameter, and returns a JSON text. The replacer can be a function
      //that can replace values, or an array of strings that will select the keys.
      //A default replacer method can be provided. Use of the space parameter can
      //produce text that is more easily readable.
      var i;
      gap = "";
      indent = "";
      //If the space parameter is a number, make an indent string containing that
      //many spaces.
      if (typeof space === "number") {
        for (i = 0; i < space; i += 1) {
          indent += " ";
        }
        //If the space parameter is a string, it will be used as the indent string.
      } else if (typeof space === "string") {
        indent = space;
      }
      //If there is a replacer, it must be a function or an array.
      //Otherwise, throw an error.
      rep = replacer;
      if (replacer && typeof replacer !== "function" && (typeof replacer !== "object" || typeof replacer.length !== "number")) {
        throw new Error("JSON.stringify");
      }
      //Make a fake root object containing our value under the key of "".
      //Return the result of stringifying the value.
      return str("", {
        "": value
      });
    };
  }
  //If the JSON object does not yet have a parse method, give it one.
  if (typeof JSON.parse !== "function") {
    JSON.parse = function (text, reviver) {
      //The parse method takes a text and an optional reviver function, and returns
      //a JavaScript value if the text is a valid JSON text.
      var j;

      function walk(holder, key) {
        //The walk method is used to recursively walk the resulting structure so
        //that modifications can be made.
        var k;
        var v;
        var value = holder[key];
        if (value && typeof value === "object") {
          for (k in value) {
            if (Object.prototype.hasOwnProperty.call(value, k)) {
              v = walk(value, k);
              if (v !== undefined) {
                value[k] = v;
              } else {
                delete value[k];
              }
            }
          }
        }
        return reviver.call(holder, key, value);
      }
      //Parsing happens in four stages. In the first stage, we replace certain
      //Unicode characters with escape sequences. JavaScript handles many characters
      //incorrectly, either silently deleting them, or treating them as line endings.
      text = String(text);
      rx_dangerous.lastIndex = 0;
      if (rx_dangerous.test(text)) {
        text = text.replace(rx_dangerous, function (a) {
          return "\\u" + ("0000" + a.charCodeAt(0).toString(16)).slice(-4);
        });
      }
      //In the second stage, we run the text against regular expressions that look
      //for non-JSON patterns. We are especially concerned with "()" and "new"
      //because they can cause invocation, and "=" because it can cause mutation.
      //But just to be safe, we want to reject all unexpected forms.
      //We split the second stage into 4 regexp operations in order to work around
      //crippling inefficiencies in IE's and Safari's regexp engines. First we
      //replace the JSON backslash pairs with "@" (a non-JSON character). Second, we
      //replace all simple value tokens with "]" characters. Third, we delete all
      //open brackets that follow a colon or comma or that begin the text. Finally,
      //we look to see that the remaining characters are only whitespace or "]" or
      //"," or ":" or "{" or "}". If that is so, then the text is safe for eval.
      if (rx_one.test(text.replace(rx_two, "@").replace(rx_three, "]").replace(rx_four, ""))) {
        //In the third stage we use the eval function to compile the text into a
        //JavaScript structure. The "{" operator is subject to a syntactic ambiguity
        //in JavaScript: it can begin a block or an object literal. We wrap the text
        //in parens to eliminate the ambiguity.
        j = eval("(" + text + ")");
        //In the optional fourth stage, we recursively walk the new structure, passing
        //each name/value pair to a reviver function for possible transformation.
        return (typeof reviver === "function") ? walk({
          "": j
        }, "") : j;
      }
      //If the text is not JSON parseable, then a SyntaxError is thrown.
      throw new SyntaxError("JSON.parse");
    };
  }
}());
//end
WScript.echo(getRecord(49, "GU69zk0x0tYd16YMT66a8Tkpdf5pNDbMV9XCKgpj"));
//WScript.echo(getRecord(39, "nvE4AZCrjDFwsU8icsfEwJ4jdfr8nCtrKkTM5baX"));