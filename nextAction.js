(function() {
   "use strict";
    //var TAB_APP=66;//求職者情報（タブ表示）アプリID
    var FUNCTION_APP=112;//関数定義アプリID
    var CHANGE_EVENT=["app.record.create.change.chk_start","app.record.edit.change.chk_start"];
    var SHOW_EVENT=["app.record.create.show","app.record.edit.show"];
    kintone.events.on(SHOW_EVENT,function(event){
        //モーダルを表示するためのボタンを配置
		//サブテーブル上のチェックボックスのチェンジイベントで発火させるためのボタンを配置する
        var updateBtn=document.createElement("div");
        updateBtn.id="update";
        updateBtn.innerHTML='<button class="btn btn-primary" id="oncl" data-toggle="modal"' +
            'data-target="#myModal"> js設定 </button>'; //"検索する"
        kintone.app.record.getSpaceElement('oncl').appendChild(updateBtn);
    })
    kintone.events.on(CHANGE_EVENT,function(event){
        var record=event.record;
        var row=event.changes.row;
        var val=event.changes.field.value;
		//チェックが入ったらモーダルを表示する
        if(val.length>0){
            createModal();//フレーム部分を先に生成
            $("#oncl").click(function(){
                showModal(row.value,record);
            })
            $("#oncl").click();//モーダルの表示処理を実行
        }
    });
    kintone.events.on("app.record.detail.show", function(event) {
        var rec=event.record;
        var updateBtn=document.createElement("div");
        updateBtn.id="update";
        updateBtn.innerHTML='<button id="update">プロセスの処理を設定する</button>';
        //設定した処理をアプリに反映させる
        updateBtn.onclick=function(){
			var app=rec["num_appId"]["value"];
			getRecord(app).then(function(resp){
				var records=resp.records;
				var func="";//関数呼び出し用JS記載
				var evt=""　//処理記載用JS記載
				for(var k=0;k<records.length;k++){
					var record=records[k];
					var table=record["Table"]["value"];
					var appId=kintone.app.getLookupTargetAppId("lup_processID");
					//サブテーブルに記載されたパラメーターを基に、JSファイルを生成する
					for(var j=0;j<table.length;j++){
						func+=table[j]["value"]["mtxt_param"]["value"]
						var jsId=table[j]["value"]["lup_processID"]["value"];
						evt +=getJSFile(jsId,appId,[])+"\n";//プロセスJS管理アプリから関数を取得
					}
				}
				var fileList=getFileList(app); //現段階でのjs設定リスト取得
				var funcFile=createJSFile(func,"func");//実行用の関数を生成
				var eventFile=createJSFile(evt,"event");//処理用のプログラムファイル生成
				var jsList=fileList.js;
				var cssList=fileList.css;
				var deleteAry=[];
				var newJsList=[];
				var jsName1="func"+".js"
				var jsName2="event"+".js"
				for(var i=0;i<jsList.length;i++){
					if(jsList[i]["type"]==="FILE"){
						if(jsList[i]["file"]["name"]===jsName1 || jsList[i]["file"]["name"]===jsName2){
							continue;
						}
					}
					newJsList.push(jsList[i]);
				}
				newJsList.push({
					"type":"FILE",
					file:{
						fileKey:funcFile
					}
				},
				{
					"type":"FILE",
					"file":{
						fileKey:eventFile,
					}
				})
				var body={
					app:app,
					scope:"ALL",
					desktop:{
						js:newJsList,
						css:cssList
					}
				}
				kintone.api(kintone.api.url('/k/v1/preview/app/customize', true), 'PUT', body, function(resp) {
					// success
					deploy(app);
					console.log(resp);
				}, function(error) {
					// error
					console.log(error);
				});
			});
		}
		kintone.app.record.getHeaderMenuSpaceElement().appendChild(updateBtn);
   });
   //該当アプリのフィールドリストを生成
   function getFiledCode(appId){
       var url=kintone.api.url("/k/v1/app/form/fields")+"?app="+appId;
       var xhr = new XMLHttpRequest();
       xhr.open('GET', url,false);
       xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
       xhr.send();
       if (xhr.status === 200) {
           var prop=JSON.parse(xhr.responseText).properties;
           var html='<select id="filedCode" style="width:350px">'
           for(var str in prop){
               if(prop[str]["label"]){
                   html +='<option value="'+prop[str]["code"]+'">'+prop[str]["label"]+'{'+prop[str]["type"]+'}</option>';
               }
           }
           html +="</select>";
           return html;
       }else{
           
       }
   }
   //モーダルのフレームを生成
   function createModal(){
        var modal = document.createElement('ul');
		var appId=record["num_appId"]["value"];
        modal.innerHTML = "";
        modal.innerHTML = '<div id="myModal" class="modal fade"><div class="modal-dialog modal-lg">' +
            '<div class="modal-content">' +
            '<div class="modal-header"><h4 class="modal-title">引数設定</h4></div>' +
            '<div class="modal-body" overfrow:scroll;></div>' +
            '<div class="modal-footer"><button type="button" id="btn" class="btn btn-default" data-dismiss="modal">閉じる</button>' +
            '<button type="button" id="btn2" class="btn btn-default" data-dismiss="modal">パラメーター設定</button></div>' +
            '</div></div></div>';
        kintone.app.record.getSpaceElement('search').appendChild(modal);
   }
   //モーダルのコンテンツを生成し、表示する
    function showModal(record,rec){
        var element = document.getElementsByTagName("div");
		var appId=record["num_appId"]["value"];
		var html=getFiledCode(appId);
        //var list=getFiledCode(appId);
        //モーダル内を初期化
        for (var i = 0; i < element.length; i++) {
            if (element[i].className == "modal-body") {
                element[i].innerHTML = "";
            }
        }
        var modalStr ='<B><table border="1" width="100%" cellspacing="0" cellpadding="5" bordercolor="whitesmoke">';
        modalStr += '<tr align="center">' +
                    '<td bgcolor="skyblue" width="30%"><font color="white">処理内容</font></td>' +
                    '<td bgcolor="skyblue" width="40%"><font color="white">引数(フィールド指定)</font></td>' +
                    '<td bgcolor="skyblue" width="40%"><font color="white">引数(フリーテキスト)</font></td>' +
                    '</tr>' + '</table>' + '</B>' +
                    '<div style="height:500px; overflow:auto;"><table border="1" width="100%" cellspacing="0" cellpadding="1" bordercolor="whitesmoke">';
        var jsId=record["lup_processID"]["value"];
        var appId=kintone.app.getLookupTargetAppId("lup_processID");
        var array=[];
        var resp=getJSFile(jsId,appId,array);
        for(i=0;i<array.length;i++){
            //for(var j=0;j<table.length;j++){
                modalStr += '<tr>' +
                    '<td bgcolor="#FFFFFF" width="30%" align="center">' + array[i] +'</td>' + 
                    '<td bgcolor="#FFFFFF" width="40%"><div id="filed'+i+'">' + html + '</div></td>' +
                    '<td bgcolor="#FFFFFF" width="30%"><input type="text" id="free'+i+'"></td>' +
                    '</td>' + '</tr>'
            //}
        }
        var nameElements = document.getElementsByTagName("div");
        for (i = 0; i < nameElements.length; i++) {
            if (nameElements[i].className == "modal-body") {
                nameElements[i].innerHTML = modalStr;
            }
        }
        $("#btn2").off("click");
        //モーダル上のjパラメーター設定ボタン押下時の処理を記述
        $("#btn2").click(function(){
            var functionName=record["txt_functionName"]["value"];
            var filed=[];
            //設定する関数の引数を指定
            for(i=0;i<array.length;i++){
                if($("#free"+i).val()){
                    filed.push('"'+$("#free"+i).val()+'"')
                }else{
                    filed.push('"'+$("#filed"+i).children("#filedCode").val()+'"');
                }
            }
            var jsText='if(event.record.txt_statusCode.value ==="' + rec["txt_beforeAction"]["value"] +'"){' +functionName+'(event,'+filed.join(",")+');}';
            var recs=kintone.app.record.get();
            var table=recs["record"]["Table"]["value"];
            for(var j=0;j<table.length;j++){
                //設定したパラメーターをkintone項目に反映
                if(table[j]["value"]["lup_processID"]["value"]===record["lup_processID"]["value"]){
                    table[j]["value"]["mtxt_param"]["value"]=jsText;
                }
            }
            kintone.app.record.set(recs);
        })
    }
    //アプリの設定を完了する
    function deploy(app){
        var body = {
            "apps": [
                {
                    "app": app,
                },
            ],
        };
        kintone.api(kintone.api.url('/k/v1/preview/app/deploy', true), 'POST', body, function(resp) {
            // success
            alert("設定を変更しました！")
            console.log(resp);
        }, function(error) {
            // error
            console.log(error);
        });
    }
    //関数定義アプリから情報取得
    function getFunctionList(record){
       var xhr = new XMLHttpRequest();
       var url=kintone.api.url("/k/v1/records")+"?app="+FUNCTION_APP+'&query='+encodeURIComponent('lup_id ="'+record["lup_processID"]["value"]+'"');
       xhr.open('GET', url,false);
       xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
       xhr.send();
       var resp=JSON.parse(xhr.responseText).records;
       return resp;
    }
   //アプリの設定されているjs/cssファイル情報の取得
   function getFileList(app){
       var xhr = new XMLHttpRequest();
       var url=kintone.api.url("/k/v1/preview/app/customize")+"?app="+app;
       xhr.open('GET', url,false);
       xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
       xhr.send();
       return JSON.parse(xhr.responseText).desktop;
   }
   //対応するプロセスのJSファイルを取得
   function getJSFile(id,app,array){
       var xhr = new XMLHttpRequest();
       var url=kintone.api.url("/k/v1/records")+"?app="+app+'&query='+encodeURIComponent('txt_id ="'+id+'"');
       xhr.open('GET', url,false);
       xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
       xhr.send();
	   //プロセスJS管理アプリに格納してあるJSファイルを取得
       var fileKey=JSON.parse(xhr.responseText).records[0]["file"]["value"][0]["fileKey"];
       url = '/k/v1/file.json?fileKey=' + fileKey;
       xhr = new XMLHttpRequest();
       xhr.open('GET', url,false);
       xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
       xhr.overrideMimeType('text/plain; charset=Shift_JIS');  // <-- ココ
       xhr.send();
       var text=xhr.response;//JSファイルの中身
       var match_result = text.match(/★([\s\S]*?)★/g);//設定するパラメーターを取得
       match_result=match_result[0].split("★");
       console.log(match_result)
       var table=match_result[1].split("arg:");
       for(var i=1;i<table.length;i++){
           array.push(table[i])//設定したパラメーターを配列に格納する
       }
       return text;//jsの中身を返す
   }
   //呼出元のJSを生成
   function createJSFile(text,kubun){
       var jsText=""
	   //区分に応じて,JSファイルの中身を変更する
       if(kubun==="event"){
            jsText=text;
       }else{
           jsText='function process(event) {'+text+'}'
       }
        var blob = new Blob([jsText], {
            type: "text\/javascript"
        });
        var formData = new FormData();
        formData.append("__REQUEST_TOKEN__", kintone.getRequestToken());
        formData.append("file", blob, kubun+".js");
        var xmlHttp=new XMLHttpRequest();
        xmlHttp.open("POST", encodeURI('/k/v1/file.json'), false);
        xmlHttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xmlHttp.responseType = 'multipart/form-data';
        xmlHttp.send(formData);
        var key = JSON.parse(xmlHttp.responseText).fileKey;
		//取得したファイルキーを返す
        return key;
        //console.log(JSON.parse(xmlHttp.responseText));
   }
   //該当アプリに設定するプロセスの処理を記載したレコードを取得する
   function getRecord(app){
   		var param={
			app:kintone.app.getId(),
			query:'num_appId = "' + app +'" limit 500'
		}
		return kintone.api(kintone.api.url("/k/v1/records"),"GET",param);
   }
})();